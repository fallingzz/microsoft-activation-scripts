   **Microsoft Activation Scripts (MAS):**

   A collection of scripts for activating Microsoft products using HWID / KMS38 / Online KMS activation methods 
   with a focus on open-source code, less antivirus detection and user-friendliness.

   **Homepages:**<br/>
   NsaneForums: (Login Required) https://www.nsaneforums.com/topic/316668-microsoft-activation-scripts/<br/>
   GitHub: https://github.com/massgravel/Microsoft-Activation-Scripts<br/>
   GitLab: https://gitlab.com/massgrave/microsoft-activation-scripts<br/>
   
  Latest Version: 1.4<br/>
  Release date: 14-aug-2020

  # **Downloads:** <br/>
   https://gitlab.com/massgrave/microsoft-activation-scripts/-/releases

<br/> 

<pre class="ipsCode prettyprint lang-html prettyprinted"><span class="pln">----------------------------------------------------------------------------------------------
Activation Type       Supported Product             Activation Period
----------------------------------------------------------------------------------------------

Digital License    -  Windows 10                 -  Permanent
KMS38              -  Windows 10 / Server        -  Until the year 2038
Online KMS         -  Windows / Server / Office  -  For 180 Days, renewal task needs to be 
                                                    created for lifetime auto activation.

----------------------------------------------------------------------------------------------</span></pre>

  # **ReadMe:**
   
<p>
<details>
<summary>Click me to collapse/fold.</summary>
<br/> 
<a href="https://gitlab.com/massgrave/microsoft-activation-scripts/raw/master/MAS_1.4/Separate-Files-Version/Activators/HWID-KMS38_Activation/ReadMe_HWID.txt" target="_blank">Digital License (HWID) Activation</a>
<br/> 
<a href="https://gitlab.com/massgrave/microsoft-activation-scripts/raw/master/MAS_1.4/Separate-Files-Version/Activators/HWID-KMS38_Activation/ReadMe_KMS38.txt" target="_blank">KMS38 Activation</a>
<br/>
<a href="https://gitlab.com/massgrave/microsoft-activation-scripts/raw/master/MAS_1.4/Separate-Files-Version/Extras/KMS38_Protection/ReadMe.txt" target="_blank">KMS38_Protection</a>
<br/>
<a href="https://gitlab.com/massgrave/microsoft-activation-scripts/raw/master/MAS_1.4/Separate-Files-Version/Activators/Online_KMS_Activation/_ReadMe.txt" target="_blank">Online KMS Activation</a>
<br/>
<a href="https://pastebin.com/raw/7Xyaf15Z" target="_blank">Activation Methods info and faqs</a>
<br/>
<a href="https://gitlab.com/massgrave/microsoft-activation-scripts/raw/master/MAS_1.4/Separate-Files-Version/Extras/Extract_OEM_Folder/ReadMe.txt" target="_blank">$OEM$ Folders (Windows Pre-Activation)</a>
<br/>
<a href="https://pastebin.com/raw/DdM34pr5" target="_blank">Big Blocks of text in the script</a>
<br/>
<a href="https://pastebin.com/raw/jduBSazJ" target="_blank">Download Genuine Installation Media</a>
<br/>
</details>
</p>  

# <a href="https://gitlab.com/massgrave/microsoft-activation-scripts/raw/master/MAS_1.4/Separate-Files-Version/Credits.txt" target="_blank">Credits:</a>

# <a href="https://pastebin.com/raw/nghFEt3W" target="_blank">Changelog:</a>

<ul>


For any queries feel free to mail me at, windowsaddict@protonmail.com



Made with Love ❤️
